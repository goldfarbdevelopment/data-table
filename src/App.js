import React from 'react';
import './App.css';
import Datatable from "./Datatable";
function App() {
  return (
    <div className="App">
        <div className="container">
            <Datatable/>
        </div>
    </div>
  );
}

export default App;
