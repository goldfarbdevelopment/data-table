import React, {Component} from 'react';

class AccessName extends Component {
	constructor(props) {
		super(props);

		this.state = {
			name: '',
			description: '',
			readers: ''
		}
	}

	componentDidMount () {
		this.setState({
			name: this.props.accessName.name,
			description: this.props.accessName.description,
			readers: this.props.readers,
			reader: this.props.accessName.reader
		})
	}
	render() {
		const readers = this.props.readers.map((reader, index)=> {
			return (<option value={reader.name} key={index}>{reader.name}</option>);
		});

		return (
			<form>
				<h3>Access Name</h3>
				<div className="form-group row">
					<label htmlFor="accessName" className="col-sm-2 col-form-label">Name</label>
					<div className="col-sm-10">
						<input name="name" onChange={this.props.inputHandler} type="text" className="form-control" id="accessName" value={this.props.accessName.name}/>
					</div>
				</div>
				<div className="form-group row">
					<label htmlFor="accessDescription" className="col-sm-2 col-form-label">Description</label>
					<div className="col-sm-10">
						<textarea onChange={this.props.inputHandler} name="description" className="form-control" id="accessDescription"  value={this.props.accessName.description}/>
					</div>
				</div>
				<div className="form-group row">
					<label htmlFor="accessReaders" className="col-sm-2 col-form-label">Reader(s)</label>
					<div className="col-sm-10">
						<select onChange={this.props.inputHandler} name="reader" value={this.props.accessName.reader} id="accessReaders">{readers}</select>
					</div>
				</div>
				<div className="form-group row">
					<div className="col-sm-10">
						<input type="hidden" name="id" value={this.props.accessName.id} />
						<button onClick={this.props.onFormSubmit} className="btn btn-outline-success mr-1">Save</button>
						<button onClick={this.props.clearForm} className="btn btn-outline-info">Cancel</button>
					</div>
				</div>
			</form>
		);
	}


};

export default AccessName;