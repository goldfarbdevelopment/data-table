import React, {Component, Fragment} from 'react';
import json from './json';
import AccessName from "./accessName";
class Datatable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accessLevels: json.accessLevels,
            readers: json.readers,
            readerTypes: json.readerTypes,
            spotlightInput: '',
            tableRows: [],
            rows: '',
            accessName: {}
        };

        this.clearForm = this.clearForm.bind(this);
        this.accessNameFormSubmit = this.accessNameFormSubmit.bind(this);
        this.spotLightInputHandler = this.spotLightInputHandler.bind(this);
        this.accessNameFormInputHandler = this.accessNameFormInputHandler.bind(this);
    }

    componentDidMount() {
        const tableRows = this.buildTableRows(),
            rows = this.buildTable(tableRows);
        let readers = json.readers;
        readers.unshift({
            "id": -1,
            "typeId": -1,
            "name": "All Reader Groups"
        });

        this.setState({
            readers, rows, tableRows
        });
    }

    spotLightInputHandler (e) {
        const tableFilter = this.state.tableRows.filter(row => {
            const cells = Object.keys(row);
            let match = false;
            cells.forEach(cell => {

                if(cell !== 'id' && row[cell].toString().toLowerCase().includes(e.target.value.toLowerCase())) {
                    match = true;
                }
            });
            return match;
        });

        const rows = this.buildTable(tableFilter);
        this.setState({
            rows,
            spotlightInput: e.target.value
        });
    }

    findObjectValue (val, obj) {
        return obj.find(el=>{
            return el.id === val;
        });
    }

    buildTableRows () {
        return this.state.accessLevels.map(accessLevel => {
            const description = accessLevel.Description,
                id = accessLevel.id,
                name = accessLevel.name,
                readerObj = this.findObjectValue(accessLevel.readerId, this.state.readers),
                readerTypeObj = this.findObjectValue(readerObj.typeId, this.state.readerTypes),
                readerType = readerTypeObj.name,
                reader = readerObj.name;

            return {id, description, name, readerType, reader}
        });
    }

    buildTable (rows) {
        return rows.map((row, index) => {
           return (
               <tr key={index}
                   onClick={() => {
                        this.selectRow(row);
                   }}
               >
                   <td>{row.name}</td>
                   <td>{row.readerType}</td>
                   <td>{row.reader}</td>
               </tr>);
        });
    }

    selectRow (accessName) {
        this.setState({accessName});
    }

    accessNameFormInputHandler (e) {
        const accessName = this.state.accessName;
        accessName[e.target.name] = e.target.value;
        this.setState({accessName});
    }

    accessNameFormSubmit (e) {
        e.preventDefault();
        let tableRows = this.state.tableRows;
            tableRows.indexOf(row => {
                return row.id = this.state.accessRow.id
            });
        const rows = this.buildTable(tableRows);
        this.setState({rows});
    }

    clearForm (e) {
        e.preventDefault();
        this.setState({
            accessName: {
                name: '',
                description: '',
                reader:''
            }
        })
    }
    render() {
        return (
            <Fragment>
                <h1 className="h3 border-bottom">Access Levels</h1>
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <input onChange={this.spotLightInputHandler} type="text" placeholder="Spotlight Search" value={this.state.spotlightInput}/>
                            </div>
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Reader Type</th>
                                        <th scope="col">Readers</th>
                                    </tr>
                                </thead>
                                <tbody>{ this.state.rows }</tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <AccessName
                            clearForm={this.clearForm}
                            onFormSubmit={this.accessNameFormSubmit}
                            inputHandler={this.accessNameFormInputHandler}
                            readers={this.state.readers}
                            accessName={this.state.accessName} />
                    </div>
                </div>
            </Fragment>
            );
    }
}

export default Datatable;