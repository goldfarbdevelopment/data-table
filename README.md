This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
This was a coding assignment that I did in React.  The following were the requirments, also found on UI Assessment.docx

Given following data sets, create a list detail view show in Image below.
Use any JavaScript framework of your choice.
Main goal of the page would be to show details on right side panel upon selecting a row from the list on left side.
Changes on the right side panel should be reflected in grid.
Spotlight search at top, will filter rows in grid, a substring in either Access Level Name, Reader Type, or Reader Name is a match.

UI Mockup
 

Datasets
accessLevels json
===========================
	[{
			"id": 1,
			"name": "Morning 9:00 - 10:00",
			"readerId": 10,
			"Description": "Morning Front door Access"
		},
		{
			"id": 2,
			"name": "Morning 10:00 - 11:00",
			"readerId": 11,
			"Description": ""
		},
		{
			"id": 3,
			"name": "Morning 11:00 - 12:00",
			"readerId": 12,
			"Description": ""
		},
		{
			"id": 4,
			"name": "Evening 3:00 - 4:00",
			"readerId": 13,
			"Description": ""
		},
		{
			"id": 5,
			"name": "Evening5:00 - 6:00",
			"readerId": 14,
			"Description": ""
		},
		{
			"id": 6,
			"name": "All Day Elevator",
			"readerId": 15,
			"Description": ""
		},
		{
			"id": 7,
			"name": "All Day Back Door",
			"readerId": 16,
			"Description": ""
		},
		{
			"id": 8,
			"name": "All Day Supply Door",
			"readerId": 17,
			"Description": ""
		}
	]
===========================
	"readers"
===========================
[{
			"id": 10,
			"typeId": 1,
			"name": "Reader F1"
		},
		{
			"id": 11,
			"typeId": 1,
			"name": "Reader F2"
		},
		{
			"id": 12,
			"typeId": 1,
			"name": "Reader F3"
		},
		{
			"id": 13,
			"typeId": 1,
			"name": "Reader F4"
		},
		{
			"id": 14,
			"typeId": 1,
			"name": "Reader F5"
		},
		{
			"id": 15,
			"typeId": 2,
			"name": "Reader E"
		},
		{
			"id": 16,
			"typeId": 3,
			"name": "Reader B"
		},
		{
			"id": 17,
			"typeId": 4,
			"name": "Reader S"
		}
	],
===========================
	"readerTypes"
===========================

 [{
			"id": 1,
			"name": "Front Door"
		},
		{
			"id": 2,
			"name": "Elevator Door"
		},
		{
			"id": 3,
			"name": "Back Door"
		},
		{
			"id": 4,
			"name": "Supply Door"
		}
	]
===========================



## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
